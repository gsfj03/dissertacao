\select@language {brazil}
\contentsline {chapter}{Lista de Figuras}{iv}{section*.1}
\contentsline {chapter}{Lista de Tabelas}{vii}{section*.2}
\contentsline {chapter}{\chapternumberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motiva\IeC {\c c}\IeC {\~a}o}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos e Metas}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Organiza\IeC {\c c}\IeC {\~a}o da disserta\IeC {\c c}\IeC {\~a}o}{2}{section.1.3}
\contentsline {chapter}{\chapternumberline {2}Aut\IeC {\^o}matos Celulares com Pontos Qu\IeC {\^a}nticos (QCA)}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}C\IeC {\'e}lula QCA}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Elementos B\IeC {\'a}sicos}{6}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Fio}{6}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Inversor}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Porta Majority ou Maioria}{7}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Cruzamento de Fios}{9}{subsection.2.2.4}
\contentsline {section}{\numberline {2.3}Zonas de Clock}{10}{section.2.3}
\contentsline {section}{\numberline {2.4}USE}{11}{section.2.4}
\contentsline {section}{\numberline {2.5}Posicionamento e Roteamento}{11}{section.2.5}
\contentsline {section}{\numberline {2.6}Simuladores}{12}{section.2.6}
\contentsline {chapter}{\chapternumberline {3}Trabalhos Relacionados}{14}{chapter.3}
\contentsline {chapter}{\chapternumberline {4}Algoritmo de P\&R}{19}{chapter.4}
\contentsline {section}{\numberline {4.1}Formula\IeC {\c c}\IeC {\~a}o do Problema}{19}{section.4.1}
\contentsline {section}{\numberline {4.2}Algoritmo}{20}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Estrutura de Entrada}{25}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Estruturas de Dados}{26}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Algoritmo de P\&R}{27}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Parti\IeC {\c c}\IeC {\~a}o}{30}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Sobreposi\IeC {\c c}\IeC {\~a}o de solu\IeC {\c c}\IeC {\~o}es}{32}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Caminhos Reconvergentes}{36}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}Poda de solu\IeC {\c c}\IeC {\~o}es}{38}{subsection.4.2.7}
\contentsline {section}{\numberline {4.3}Porta majority de 5 entradas}{40}{section.4.3}
\contentsline {chapter}{\chapternumberline {5}Resultados}{43}{chapter.5}
\contentsline {section}{\numberline {5.1}Compara\IeC {\c c}\IeC {\~a}o com trabalhos anteriores baseados no esquema USE}{43}{section.5.1}
\contentsline {section}{\numberline {5.2}Impacto da Topologia na \IeC {\'A}rea Final}{45}{section.5.2}
\contentsline {section}{\numberline {5.3}Avalia\IeC {\c c}\IeC {\~a}o do P\&R}{47}{section.5.3}
\contentsline {section}{\numberline {5.4}Porta majority de 5 entradas}{48}{section.5.4}
\contentsline {section}{\numberline {5.5}Poda de solu\IeC {\c c}\IeC {\~o}es}{49}{section.5.5}
\contentsline {chapter}{\chapternumberline {6}Conclus\IeC {\~a}o}{53}{chapter.6}
\contentsline {appendix}{\chapternumberline {A}Projetos de circuitos QCA}{55}{appendix.A}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{65}{section*.69}
